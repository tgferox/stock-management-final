<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    use HasFactory;
    
    protected $fillable = [
        'sku',
        'name',
        'description',
        'product_id',
    ];
}
