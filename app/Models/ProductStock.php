<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
    public function variation()
    {
        return $this->belongsTo(Variation::class);
    }
    use HasFactory;

    protected $fillable = [
        'stock_level',
        'location_id',
        'variation_id'
    ];
}
