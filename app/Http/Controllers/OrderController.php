<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;

use App\Models\Order;
use App\Models\OrdersProduct;
use App\Models\Variation;
use App\Models\Location;
use App\Models\ProductStock;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;



class OrderController extends Controller
{
    //
    public function index()
    {
        $orders = Order::all();
        
        return view('order.index', ['orders' => $orders]);
    }

    public function create()
    {
        return view('order.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string', 'max:255'],
            'county' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
            'amount_due' => ['required', 'integer'],
            'status' => ['required', 'string', 'max:255'],
            'delivery_date' => ['required', 'date', 'max:255'],
        ]);

        $order = Order::Create([
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'county' => $request->county,
            'postal_code' => $request->postal_code,
            'amount_due' => $request->amount_due,
            'status' => $request->status,
            'delivery_date' => $request->delivery_date,
        ]);

        $products = $request->products;

        return redirect()->route('orders.show', ['id' => $order->id]);
    }

    public function show(Request $request, $id)
    {
        $order = Order::where('id', $id)->first();

        $products = OrdersProduct::where('order_id', $id)->get();
        $product_list = [];

        foreach($products as $product)
        {
            $variation = Variation::where('id', $product->product_id)->first();
            Log::info($variation);
            $list_entry['name'] = $variation->name;
            $list_entry['id'] = $id;
            $list_entry['qty'] = $product->quantity;

            array_push($product_list, $list_entry);
            unset($list_entry);
        }

        return view('order.show', ['order' => $order, 'product_list' => $product_list, 'id' => $id]);
    }

    public function edit(Request $request, $id)
    {
        $order = Order::where('id', $id)->first();

        $variations = Variation::all();

        $locations = Location::all();

        $products = OrdersProduct::where('order_id', $id)->get();
        $product_list = [];

        foreach($products as $product)
        {
            $variation = Variation::where('id', $product->product_id)->first();

            $list_entry['name'] = $variation->name;
            $list_entry['id'] = $product->id;
            $list_entry['qty'] = $product->quantity;

            array_push($product_list, $list_entry);
            unset($list_entry);
        }

        return view('order.edit', ['order' => $order, 'product_list' => $product_list, 'id' => $id, 'locations' => $locations, 'variations' => $variations]);
    }

    public function update(Request $request, $id)
    {
        $products = OrdersProduct::where('order_id', $id)->get();
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string', 'max:255'],
            'county' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
            'amount_due' => ['required', 'integer'],
            'status' => ['required', 'string', 'max:255'],
            'delivery_date' => ['required', 'date', 'max:255'],
        ]);

        $updated_order = Order::Where('id', $id)
        ->update([
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'county' => $request->county,
            'postal_code' => $request->postal_code,
            'amount_due' => $request->amount_due,
            'status' => $request->status,
            'delivery_date' => $request->delivery_date,
        ]);

        return redirect()->route('orders.show', ['id' => $id]);
    }

    public function delete(Request $request, $id)
    {
        $product = Order::where('id', $id)
        ->update([
            'status' => 'Cancelled',
        ]);

        return redirect()->route('orders');
    }

    public function add_product(Request $request, $id)
    {
        $request->validate([
            'variation_id' => ['required', 'integer'],
            'quantity' => ['required', 'integer'],
            'location' => ['required', 'integer']
        ]);

        $order = OrdersProduct::Create([
            'order_id' => $id,
            'product_id' => $request->variation_id,
            'quantity' => $request->quantity,
            'location_code' => $request->location
        ]);

        $product_stock = ProductStock::where(function ($query) use ($id, $request) {
            $query->where([['variation_id', '=', $request->variation_id],
                           ['location_id', '=', $request->location]]);
            })->first();

        $product = ProductStock::where('id', $product_stock->id)
            ->update([
                'stock_level' => ($product_stock->stock_level - $request->quantity),
            ]);

        return redirect()->route('orders.show', ['id' => $id]);
    }

    public function delete_product(Request $request, $id, $product_id)
    {
        $product = OrdersProduct::where('id', $product_id)->first();

        $product_stock = ProductStock::where(function ($query) use ($product) {
            $query->where([['variation_id', '=', $product->product_id],
                           ['location_id', '=', $product->location_code]]);
            })->first();

        $product = ProductStock::where('id', $product_stock->id)
            ->update([
                'stock_level' => ($product_stock->stock_level + $product->quantity),
            ]);

        OrdersProduct::where('id', $product_id)->delete();

        return redirect()->route('orders.show', ['id' => $id]);
    }

    public function deliveries_index()
    {
        $dates = Order::distinct()->get(['delivery_date']);

        return view('delivery.index', ['dates' => $dates]);
    }

    public function deliveries_show(Request $request, $id)
    {
        $orders = Order::where('delivery_date', $id)->get();

        return view('delivery.show', ['orders' => $orders,'id' => $id]);
    }
}
