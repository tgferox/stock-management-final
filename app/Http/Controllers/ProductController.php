<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Location;
use App\Models\ProductStock;
use App\Models\Variation;
use App\Providers\RouteServiceProvider;

class ProductController extends Controller
{
    public function get_product_stock($product_id)
    {
        $variations = Variation::where('product_id', $product_id)->get();
        $total_stock = 0;

        foreach ($variations as $variation)
        {
            $variation_stock = ProductController::get_variation_stock($variation->id);
            $total_stock = $total_stock + $variation_stock;
        }

        return $total_stock;
    }

    public function get_variation_stock($variation_id) 
    {
        $stock_levels = ProductStock::where('variation_id', $variation_id)->get();
        $stock_level = 0;

        if ($stock_levels != [])
        {
            foreach ($stock_levels as $stock) 
            {
                $stock_level = $stock_level + $stock->stock_level;
            }
        }
        
        return $stock_level;
    }

    public function index()
    {
        $products = Product::all();
        $products_list = [];

        foreach ($products as $product) 
        {
            $product_stock = ProductController::get_product_stock($product->id);
            $product["stock"] = $product_stock;
            array_push($products_list, $product);
        }

        return view('product.index', ['products' => $products]);
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:511'],
            'low_stock_threshold' => ['required'],
        ]);

        $image_path = 'public/images/default.png';

        if ($request->image)
        {
            print($request);
            $image_name = date('YmdHi') . '.' . $request->image->extension();
            $image_path = 'images/' . $image_name; 
            $request->image->move(public_path('images'), $image_name);
        }

        $product = Product::Create([
            'name' => $request->name,
            'image' => $image_path,
            'description' => $request->description,
            'vendor' => $request->vendor,
            'category' => $request->category,
            'low_stock_threshold' => $request->low_stock_threshold
        ]);


        return redirect()->route('products.view', ['id' => $product->id]);
    }

    public function show(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();
        $variations = Variation::where('product_id', $id)->get();

        $variation_list = [];
        

        foreach ($variations as $variation)
        {
            $product_variation = [];

            $product_variation['id'] = $variation->id;
            $product_variation['stock'] = ProductController::get_variation_stock($variation->id);
            $product_variation['sku'] = $variation->sku;
            $product_variation['name'] = $variation->name;
            $product_variation['description'] = $variation->description;

            array_push($variation_list, $product_variation);
            unset($product_variation);
        }

        return view('product.show',['product' => $product, 'variations' => $variation_list]);
    }

    public function edit(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        $variations = Variation::where('product_id', $id)->get();

        $variation_list = [];
        

        foreach ($variations as $variation)
        {
            $product_variation = [];
            
            $product_variation['id'] = $variation->id;
            $product_variation['sku'] = $variation->sku;
            $product_variation['name'] = $variation->name;
            $product_variation['description'] = $variation->description;

            array_push($variation_list, $product_variation);
            unset($product_variation);
        }
            return view('product.update',['product' => $product, 'variations' => $variation_list]);
    }

    public function edit_stock(Request $request, $id)
    {
        $variations = Variation::where('product_id', $id)->get();
        $locations = Location::all();
        $stock_list = [];

        foreach ($variations as $variation)
        {

            foreach($locations as $location)
            {
                $stock = [];
                $stock["id"] = $variation->id;
                $stock['name'] = $variation->name;
                $stock["location_name"] = $location["name"];
                $stock["location_id"] = $location["id"];

                $product_stock = ProductStock::where("variation_id", $variation->id)->where("location_id", $location["id"])->first();

                if ($product_stock != [])
                {
                    $stock["stock_level"] = $product_stock["stock_level"];
                }
                else
                {
                    $stock["stock_level"] = 0;
                }

                array_push($stock_list, $stock);
                unset($stock);
            }   
        }

        
    
        

        return view('product.update_stock', ['stock_list' => $stock_list, 'id' => $id]);
    }

    public function update(Request $request, $id)
    {
        $variations = Variation::where('product_id', $id)->get();

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:511'],
            'low_stock_threshold' => ['required'],
        ]);

        $image_path = 'images/default.png';

        if ($request->image)
        {
            $image_name = date('YmdHi') . '.' . $request->image->extension();
            $image_path = 'images/' . $image_name; 
            $request->image->move(public_path('images'), $image_name);

            $product = Product::where('id', $id)
            ->update([
                'name' => $request->name,
                'image' => $image_path,
                'description' => $request->description,
                'vendor' => $request->vendor,
                'category' => $request->category,
                'low_stock_threshold' => $request->low_stock_threshold
            ]);
        }
        else{
            $product = Product::where('id', $id)
            ->update([
                'name' => $request->name,
                'description' => $request->description,
                'vendor' => $request->vendor,
                'category' => $request->category,
                'low_stock_threshold' => $request->low_stock_threshold
            ]);
        }
        

        foreach ($variations as $variation)
        {
            $updated_variant = Variation::where('id', $variation->id)
            ->update([
                'sku' => $request[(strval($variation['id']) . 'sku')],
                'name' => $request[(strval($variation['id']) . 'name')],
                'description' => $request[(strval($variation['id']) . 'description')],
            ]);
        }

        return redirect()->route('products.view', ['id' => $id]);
    }

    public function update_stock(Request $request, $id)
    {
        $locations = Location::all();

        $variations = Variation::where('product_id', $id)->get();

        foreach ($variations as $variation)
        {

            foreach($locations as $location)
            {
                ProductStock::updateOrCreate(
                    ['variation_id' => $variation->id,
                    'location_id' => $location->id],
                    ['stock_level' => $request[(strval($variation['id']) . 'location' . strval($location['id']))]
                ]);
            }
        }

        return redirect()->route('products.view', ['id' => $id]);
    }

    public function new_variation(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        return view('product.create_variation', ['product' => $product]);
    }

    public function create_variation(Request $request, $id)
    {
        $request->validate([
            'sku' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
        ]);

        $variation = Variation::create([
            'sku' => $request->sku,
            'name' => $request->name,
            'description' => $request->description,
            'product_id' => $id,
        ]);

        return redirect()->route('products.view', ['id' => $id]);
    }

    public function delete(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        $deleted_products = Product::where(function ($query) use ($id) {
            $query->where('id', '=', $id)->delete();
        });
        return redirect()->route('products', ['id' => $id]);
    }

    public function dashboard(Request $request)
    {
        $user = Auth::user();
        $products = Product::all();
        $out_of_stock = 0;
        $low_on_stock = 0;
        $product_list = [];

        foreach($products as $product)
        {
            $low_stock_product = [];
            $stock = ProductController::get_product_stock($product->id);

            if($stock <= $product->low_stock_threshold)
            {
                $low_stock_product["id"] = $product->id;
                $low_stock_product["name"] = $product->name;
                $low_stock_product["image"] = $product->image;
                $low_stock_product["sku"] = $product->sku;
                $low_stock_product["stock"] = $stock;
                if($stock == 0)
                {
                    $out_of_stock++;
                }
                else
                {
                    $low_on_stock++;
                }
                array_push($product_list, $low_stock_product);
                unset($low_stock_product);
            }
        }

        usort($product_list, function($a, $b) {
            return $a['stock'] <=> $b['stock'];
        });

        return view('dashboard', ['product_list' => $product_list, 'low_on_stock' => $low_on_stock, 'out_of_stock' => $out_of_stock, 'user' => $user]);
    }

    public function get_stock(Request $request, $id)
    {
        $stock_levels = ProductStock::where('variation_id', $id)->get();

        $stock_level = 0;
        $stocks = [];

        if ($stock_levels != [])
        {
            foreach ($stock_levels as $stock) 
            {
                $stock_level = $stock_level + $stock->stock_level;
                $stocks[$stock->location->name] = $stock->stock_level;
            }
            
        }
        $stocks["stock_level"] = $stock_level;
        return $stocks;
    }

    public function add_stock(Request $request, $id)
    {
        $product_stock = ProductStock::where(function ($query) use ($id, $request) {
            $query->where([['variation_id', '=', $id],
                           ['location_id', '=', $request->location_id]]);
            })->get();
        if (sizeof($product_stock) != 0)
        {
            ProductStock::where('id', $product_stock[0]->id)->update(['stock_level' => ($product_stock[0]->stock_level + $request->amount)]);
            $new_product_stock = ProductStock::where('id', $product_stock[0]->id)->first();
            return $new_product_stock;
        }
        else
        {
            $new_product_stock = ProductStock::create([
                'location_id' => $request->location_id,
                'variation_id' => $id,
                'stock_level' => $request->amount
            ]);
            return $new_product_stock;
        }
        
    }

    public function take_stock(Request $request, $id)
    {
        $product_stock = ProductStock::where(function ($query) use ($id, $request) {
            $query->where([['variation', '=', $id],
                           ['location_id', '=', $request->location_id]]);
            })->get();
        if (sizeof($product_stock) != 0)
        {
            ProductStock::where('id', $product_stock[0]->id)->update(['stock_level' => ($product_stock[0]->stock_level - $request->amount)]);
            $new_product_stock = ProductStock::where('id', $product_stock[0]->id)->first();
            return $new_product_stock;
        }
    }
}
