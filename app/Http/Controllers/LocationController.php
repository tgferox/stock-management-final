<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;
use App\Providers\RouteServiceProvider;

class LocationController extends Controller
{
    //
    public function index()
    {
        $locations = Location::all();

        return view('location.index', ['locations' => $locations]);
    }

    public function new()
    {
        return view('location.new');
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string', 'max:255'],
            'address_2' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'postcode' => ['required', 'string', 'max:255'],
            'county' => ['required', 'string', 'max:511'],
        ]);

        $location = Location::Create([
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'city' => $request->city,
            'postcode' => $request->postcode,
            'county' => $request->county,
        ]);

        return redirect()->route('locations.view', ['id' => $location->id]);
    }

    public function view(Request $request, $id)
    {
        $location = Location::where('id', $id)->first();

        return view('location.show',['location' => $location]);
    }

    public function edit(Request $request, $id)
    {
        $location = Location::where('id', $id)->first();

        return view('location.update',['location' => $location]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address_1' => ['required', 'string', 'max:255'],
            'address_2' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'postcode' => ['required', 'string', 'max:255'],
            'county' => ['required', 'string', 'max:511'],
        ]);

        $updated_location = Location::where('id', $id)->update([
            'name' => $request->name,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'city' => $request->city,
            'postcode' => $request->postcode,
            'county' => $request->county,
        ]);

        return redirect()->route('locations.view', ['id' => $id]);
    }

    public function delete(Request $request, $id)
    {
        $deleted_location = Location::where('id', $id)->delete();

        return redirect()->route('locations');
    }
}
