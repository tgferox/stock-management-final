<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware('guest')
                ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('guest');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

//Get Routes
Route::get('/dashboard', [ProductController::class, 'dashboard'])
                ->middleware('auth')
                ->name('dashboard');

Route::get('/products', [ProductController::class, 'index'])
                ->middleware('auth')
                ->name('products');

Route::get('/products/new', [ProductController::class, 'create'])
                ->middleware('auth')
                ->name('products.new');

Route::get('/products/{id}/new_variation', [ProductController::class, 'new_variation'])
                ->middleware('auth')
                ->name('products.new_variation');

Route::get('/products/{id}', [ProductController::class, 'show'])
                ->middleware('auth')
                ->name('products.view');

Route::get('/products/{id}/edit', [ProductController::class, 'edit'])
                ->middleware('auth')
                ->name('products.edit');

Route::get('/products/{id}/edit_stock', [ProductController::class, 'edit_stock'])
                ->middleware('auth')
                ->name('products.edit_stock');

Route::get('/locations', [LocationController::class, 'index'])
                ->middleware('auth')
                ->name('locations');

Route::get('/locations/new', [LocationController::class, 'new'])
                ->middleware('auth')
                ->name('locations.new');

Route::get('/locations/{id}', [LocationController::class, 'view'])
                ->middleware('auth')
                ->name('locations.view');

Route::get('/locations/{id}/edit', [LocationController::class, 'edit'])
                ->middleware('auth')
                ->name('locations.edit');

Route::get('/orders', [OrderController::class, 'index'])
                ->middleware('auth')
                ->name('orders');

Route::get('/orders/new', [OrderController::class, 'create'])
                ->middleware('auth')
                ->name('orders.new');

Route::get('/orders/{id}', [OrderController::class, 'show'])
                ->middleware('auth')
                ->name('orders.show');

Route::get('/orders/{id}/edit', [OrderController::class, 'edit'])
                ->middleware('auth')
                ->name('orders.edit');

Route::get('/deliveries', [OrderController::class, 'deliveries_index'])
                ->middleware('auth')
                ->name('deliveries');

Route::get('/deliveries/{id}', [OrderController::class, 'deliveries_show'])
                ->middleware('auth')
                ->name('deliveries.show');




//Post Routes
Route::post('/products', [ProductController::class, 'store'])
                ->middleware('auth')
                ->name('products.store');

Route::post('/products/{id}/create_variation', [ProductController::class, 'create_variation'])
                ->middleware('auth')
                ->name('products.create_variation');

Route::post('/products/{id}/update', [ProductController::class, 'update'])
                ->middleware('auth')
                ->name('products.update');

Route::post('/products/{id}/update_stock', [ProductController::class, 'update_stock'])
                ->middleware('auth')
                ->name('products.update_stock');

Route::post('/locations', [LocationController::class, 'create'])
                ->middleware('auth')
                ->name('locations.create');

Route::post('/locations/{id}/update', [LocationController::class, 'update'])
                ->middleware('auth')
                ->name('locations.update');

Route::post('/orders', [OrderController::class, 'store'])
                ->middleware('auth')
                ->name('orders.store');

Route::post('/orders/{id}/products', [OrderController::class, 'add_product'])
                ->middleware('auth')
                ->name('orders.add_product');

Route::post('/orders/{id}/update', [OrderController::class, 'update'])
                ->middleware('auth')
                ->name('orders.update');

//Delete Routes
Route::delete('/products/{id}/delete', [ProductController::class, 'delete'])
                ->middleware('auth')
                ->name ('products.delete');

Route::delete('/locations/{id}/delete', [LocationController::class, 'delete'])
                ->middleware('auth')
                ->name ('locations.delete');

Route::delete('/orders/{id}/products/{product_id}/delete', [OrderController::class, 'delete_product'])
                ->middleware('auth')
                ->name ('orders.delete_product');
