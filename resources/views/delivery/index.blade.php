<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Delivery Dates') }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="font-semibold text-lg text-gray-800">See all deliveries below</h2>
                    <div class="mt-2">
                      @foreach ($dates as $date)
                      
                        <div class="grid grid-cols-10 items-center">
                            <div class="col-span-4 mb-2">
                                <p class="text-md font-medium">{{ $date->delivery_date }}</p>
                            </div>
                            <div>
                                <a href="{{ route('deliveries.show', ['id' => $date->delivery_date]) }}">
                                    <span class="text-md px-3 py-1 rounded-md bg-green-500 text-indigo-50 font-semibold cursor-pointer">View</span>
                                </a>
                            </div>
                        </div>

                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>