<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-delivery-view>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">View Deliveries for {{ $id }}</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>
        <div class="mt-4 mb-4">
            <div class='grid grid-cols-9'>
                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Name</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Address 1</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Address 2</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">County</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Postcode</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Amount Due</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Status</p>
                </div>

                <div class="mb-2">
                    <p class="block font-medium text-lg text-gray-700">Delivery Date</p>
                </div>

                <div class="mb-2">      
                </div>
            </div>
            @foreach($orders as $order)
                <div class='grid grid-cols-9 gap-4'>
                    <div class="mb-2">
                        <p>{{ $order->name }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->address_1 }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->address_2 }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->county }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->postal_code }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->amount_due }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->status }}<p/>
                    </div>

                    <div class="mb-2">
                        <p>{{ $order->delivery_date }}<p/>
                    </div>

                    <div class="mb-2">
                        <a href="{{ route('orders.edit', ['id' => $order->id]) }}">
                            <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit Order</span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>   
    </x-delivery-view>
</x-app-layout>
