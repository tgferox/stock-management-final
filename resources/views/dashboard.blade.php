<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
        <p>
            Hello {{$user["name"]}}
        </p>
    </x-slot>

    <div class="py-4">
        <div class="grid sm:grid-cols-1 md:grid-cols-4 pb-4">
            <div></div>
            <div class="max-w-md mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200 w-40 text-center">
                        <p>{{ $low_on_stock }}</p>
                        <p>Low Stock</p>
                        <p>Products</p>
                    </div>
                </div>
            </div>
            <div class="max-w-md mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-6 bg-white border-b border-gray-200 w-40 text-center">
                        <p>{{ $out_of_stock }}</p>
                        <p>Out of Stock</p>
                        <p>Products</p>
                    </div>
                </div>
            </div>
            <div></div>
        </div>
        <div class="max-w-7xl mx-auto sm:px-4 lg:px-6">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                <h2 class="font-semibold text-lg text-gray-800">See low stock products below</h2>
                    <div class="mt-2">
                      @foreach ($product_list as $product)
                      
                        <div class="grid grid-cols-3 items-center">
                            <div class="">
                                <img height='100px' src='{{ asset($product["image"]) }}' alt='{{ $product["name"] }}'/>
                            </div>
                            <div class="mb-2">
                                <p class="text-md font-medium">{{ $product["name"] }}</p>
                            </div>
                            <div class="justify-self-end">
                                <a href="{{ route('products.edit_stock', ['id' =>$product['id']]) }}">
                                    @if($product['stock'] == 0)
                                        <span class="text-md px-3 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold">Out of Stock</span>
                                    @else
                                        <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold">{{ $product['stock'] }} In Stock</span>
                                    @endif
                                </a>
                            </div>
                        </div>

                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
