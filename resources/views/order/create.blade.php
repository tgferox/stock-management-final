<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
    <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">New Order</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('orders.store') }}">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required />
            </div>

            <!-- Address 1 -->
            <div class="mt-4">
                <x-label for="address_1" :value="__('Address 1')" />

                <x-input id="address_1" class="block mt-1 w-full" type="text" name="address_1" :value="old('address_1')" required /> 
            </div>

            <!-- Address 2 -->
            <div class="mt-4">
                <x-label for="address_2" :value="__('Address 2')" />

                <x-input id="address_2" class="block mt-1 w-full" type="text" name="address_2" :value="old('address_2')" required /> 
            </div>

            <!-- County -->
            <div class="mt-4">
                <x-label for="county" :value="__('County')" />

                <x-input id="county" class="block mt-1 w-full" type="text" name="county" :value="old('county')" required />
            </div>

            <!-- Postal Code -->
            <div class="mt-4">
                <x-label for="postal_code" :value="__('Postal Code')" />

                <x-input id="postal_code" class="block mt-1 w-full" type="text" name="postal_code" :value="old('postal_code')" required />
            </div>

            <!-- Amount Due -->
            <div class="mt-4">
                <x-label for="amount_due" :value="__('Amount Due')" />

                <x-input id="amount_due" class="block mt-1 w-full" type="number" name="amount_due" :value="old('amount_due')" required />
            </div>

            <!-- Status -->
            <div class="mt-4">
                <x-label for="status" :value="__('Status')" />

                
                <select name='status'>
                    <option value='Paid'>Paid</option>
                    <option value='Due'>Due</option>
                </select>
            </div>

            <!-- Status -->
            <div class="mt-4">
                <x-label for="delivery_date" :value="__('Delivery Date')" />

                <x-input id="delivery_date" class="block mt-1 w-full" type="date" name="delivery_date" :value="old('delivery_date')" required />
            </div>


            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Create Order') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
