<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">View Order</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>
        <div class="mb-4">
            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Name</p>

                <p>{{ $order->name }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Address 1</p>

                <p>{{ $order->address_1 }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Address 2</p>

                <p>{{ $order->address_2 }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">County</p>

                <p>{{ $order->county }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Postcode</p>

                <p>{{ $order->postal_code }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Amount Due</p>

                <p>{{ $order->amount_due }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Status</p>

                <p>{{ $order->status }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Delivery Date</p>

                <p>{{ $order->delivery_date }}<p/>
            </div>

            <div class="mb-2">
                <a href="{{ route('orders.edit', ['id' => $order->id]) }}">
                    <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit Order</span>
                </a>
            </div>
        </div>   
            <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">Product Variations</h2>

            <div class="mt-4">
                @if($product_list != [])
                    @foreach($product_list as $product)
                    <div class="grid grid-cols-2 items-center mb-2">
                                    <div class="mb-2 col-span-2">
                                        <p class="text-md font-medium">Name: {{ $product['name'] }} &nbsp;&nbsp;&nbsp;&nbsp; Qty: {{ $product['qty'] }}</p>
                                    </div>
                                </div>
                    @endforeach
                @else
                    <p>This order currently has no products</p>
                @endif
            </div>
    </x-auth-card>
</x-app-layout>
