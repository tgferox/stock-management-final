<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-order-view>
    <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">New Order</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('orders.update', ['id' => $order->id]) }}">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$order->name" required />
            </div>

            <!-- Address 1 -->
            <div class="mt-4">
                <x-label for="address_1" :value="__('Address 1')" />

                <x-input id="address_1" class="block mt-1 w-full" type="text" name="address_1" :value="$order->address_1" required /> 
            </div>

            <!-- Address 2 -->
            <div class="mt-4">
                <x-label for="address_2" :value="__('Address 2')" />

                <x-input id="address_2" class="block mt-1 w-full" type="text" name="address_2" :value="$order->address_2" required /> 
            </div>

            <!-- County -->
            <div class="mt-4">
                <x-label for="county" :value="__('County')" />

                <x-input id="county" class="block mt-1 w-full" type="text" name="county" :value="$order->county" required />
            </div>

            <!-- Postal Code -->
            <div class="mt-4">
                <x-label for="postal_code" :value="__('Postal Code')" />

                <x-input id="postal_code" class="block mt-1 w-full" type="text" name="postal_code" :value="$order->postal_code" required />
            </div>

            <!-- Amount Due -->
            <div class="mt-4">
                <x-label for="amount_due" :value="__('Amount Due')" />

                <x-input id="amount_due" class="block mt-1 w-full" type="number" name="amount_due" :value="$order->amount_due" required />
            </div>

            <!-- Status -->
            <div class="mt-4">
                <x-label for="status" :value="__('Status')" />

                <x-input id="status" class="block mt-1 w-full" type="text" name="status" :value="$order->status" required />
            </div>

            <!-- Status -->
            <div class="mt-4">
                <x-label for="delivery_date" :value="__('Delivery Date')" />

                <x-input id="delivery_date" class="block mt-1 w-full" type="date" name="delivery_date" :value="$order->delivery_date" required />
            </div>

            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Update Order') }}
                </x-button>
            </div>
        </form>

        @if($product_list != [])
            @foreach($product_list as $product)
                <div class="grid grid-cols-2 items-center mb-2">
                    <div class="mb-2 col-span-2">
                        <p class="text-md font-medium">Name: {{ $product['name'] }} &nbsp;&nbsp;&nbsp;&nbsp; Qty: {{ $product['qty'] }}</p>
                    </div>
                    <div>
                        <form action="{{ route('orders.delete_product', ['id' => $order->id, 'product_id' => $product['id']]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="text-md px-6 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold cursor-pointer" title="Delete">Delete</button>
                        </form>
                    </div>
                </div>
            @endforeach
        @else
            <p>This order currently has no products</p>
        @endif

            
        <form method="POST" action="{{ route('orders.add_product', ['id' => $order->id]) }}">
            @csrf
            <div class='mt-4'>
                <select name='variation_id' required>
                    @foreach($variations as $variation)
                        <option value="{{ $variation['id'] }}">{{ $variation['name'] }}</option>
                    @endforeach
                </select>
                <label for='quantity'>Qty</label>
                <input type='number' name='quantity' id='quantity' required/>
                <select name='location' required>
                    @foreach($locations as $location)
                        <option value="{{ $location['id'] }}">{{ $location['name'] }}</option>
                    @endforeach
                </select>
                <x-button class="ml-4">
                    {{ __('Add Product Order') }}
                </x-button>  
            </div>
        </form>   
    </x-order-view>
</x-app-layout>
