<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">Edit Location</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('locations.update', ['id' => $location->id]) }}">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />
                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$location['name']" required />
            </div>

            <!-- Description -->
            <div class="mt-4">
                <x-label for="address_1" :value="__('Address 1')" />
                <x-input id="address_1" class="block mt-1 w-full" type="text" name="address_1" :value="$location['address_1']" required />
            </div>

            <div class="mt-4">
                <x-label for="address_2" :value="__('Address 2')" />
                <x-input id="address_2" class="block mt-1 w-full" type="text" name="address_2" :value="$location['address_2']" required />
            </div>

            <div class="mt-4">
                <x-label for="city" :value="__('City')" />
                <x-input id="city" class="block mt-1 w-full" type="text" name="city" :value="$location['city']" required />
            </div>

            <div class="mt-4">
                <x-label for="postcode" :value="__('Postcode')" />
                <x-input id="postcode" class="block mt-1 w-full" type="text" name="postcode" :value="$location['postcode']" required />
            </div>

            <div class="mt-4">
                <x-label for="county" :value="__('County')" />
                <x-input id="county" class="block mt-1 w-full" type="text" name="county" :value="$location['county']" required />
            </div>


            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Update Location') }}
                </x-button>
            </div>
        </form>
        <div class="flex items-center justify-end mt-4">
            <form action="{{ route('locations.delete', ['id' => $location->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button class="text-md px-6 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold cursor-pointer" title="Delete">Delete</button>
            </form>
        </div>
    </x-auth-card>
</x-app-layout>
