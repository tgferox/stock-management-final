<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">New Location</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('locations.create') }}">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />
                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required />
            </div>

            <!-- Description -->
            <div class="mt-4">
                <x-label for="address_1" :value="__('Address 1')" />
                <x-input id="address_1" class="block mt-1 w-full" type="text" name="address_1" :value="old('address_1')" required />
            </div>

            <div class="mt-4">
                <x-label for="address_2" :value="__('Address 2')" />
                <x-input id="address_2" class="block mt-1 w-full" type="text" name="address_2" :value="old('address_2')" required />
            </div>

            <div class="mt-4">
                <x-label for="city" :value="__('City')" />
                <x-input id="city" class="block mt-1 w-full" type="text" name="city" :value="old('city')" required />
            </div>

            <div class="mt-4">
                <x-label for="postcode" :value="__('Postcode')" />
                <x-input id="postcode" class="block mt-1 w-full" type="text" name="postcode" :value="old('postcode')" required />
            </div>

            <div class="mt-4">
                <x-label for="county" :value="__('County')" />
                <x-input id="county" class="block mt-1 w-full" type="text" name="county" :value="old('county')" required />
            </div>


            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Create Location') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
