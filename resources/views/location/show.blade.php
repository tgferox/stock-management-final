<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">View Location</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>
        <div class="mb-4">
            <div>
                <p class="block font-medium text-lg text-gray-700">Name</p>

                <p>{{ $location->name }}<p/>
            </div>

            <div>
                <p class="block font-medium text-lg text-gray-700">Address 1</p>

                <p>{{ $location->address_1 }}<p/>
            </div>

            <div>
                <p class="block font-medium text-lg text-gray-700">Address 2</p>

                <p>{{ $location->address_2 }}<p/>
            </div>

            <div>
                <p class="block font-medium text-lg text-gray-700">City</p>

                <p>{{ $location->city }}<p/>
            </div>

            <div>
                <p class="block font-medium text-lg text-gray-700">Postcode</p>

                <p>{{ $location->postcode }}<p/>
            </div>

            <div>
                <p class="block font-medium text-lg text-gray-700">County</p>

                <p>{{ $location->county }}<p/>
            </div>

            <div class="mt-2">
                <a href="{{ route('locations.edit', ['id' => $location->id]) }}">
                    <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit Product</span>
                </a>
            </div>
    </x-auth-card>
</x-app-layout>
