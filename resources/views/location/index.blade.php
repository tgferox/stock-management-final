<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Locations') }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="{{ route('locations.new') }}"><span class="text-md px-3 py-1 rounded-md bg-gray-500 text-indigo-50 font-semibold cursor-pointer">New Location</span></a>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="font-semibold text-lg text-gray-800">See all locations below</h2>
                    <div class="mt-2">
                      @foreach ($locations as $location)
                      
                        <div class="grid grid-cols-10 items-center">
                            <div class="col-span-8 mb-2">
                                <p class="text-md font-medium">{{ $location->name }}</p>
                            </div>
                            <div>
                                <a href="{{ route('locations.view', ['id' => $location->id]) }}">
                                    <span class="text-md px-3 py-1 rounded-md bg-green-500 text-indigo-50 font-semibold cursor-pointer">View</span>
                                </a>
                            </div>
                            <div>
                                <a href="{{ route('locations.edit', ['id' => $location->id]) }}">
                                    <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit</span>
                                </a>
                            </div>
                        </div>

                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>