<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <a href="{{ route('products.new') }}"><span class="text-md px-3 py-1 rounded-md bg-gray-500 text-indigo-50 font-semibold cursor-pointer">New Product</span></a>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h2 class="font-semibold text-lg text-gray-800">See all products below</h2>
                    <div class="mt-2">
                      @foreach ($products as $product)
                      
                        <div class="grid grid-cols-3 items-center gap-3">
                            <div class="">
                            <img height='100px' src="{{ asset($product->image) }}" alt="{{ $product->name }}"/>
                            </div>
                            <div class="mb-2">
                                <p class="text-md font-medium">
                                    {{ $product->name }}
                                </p>
                            </div>
                            <div class='grid grid-cols-3 items-center gap-3'>
                                <div>
                                    <a href="{{ route('products.view', ['id' => $product->id]) }}">
                                        <span class="text-md px-3 py-1 rounded-md bg-green-500 text-indigo-50 font-semibold cursor-pointer">View</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ route('products.edit', ['id' => $product->id]) }}">
                                        <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit</span>
                                    </a>
                                </div>
                                <div>
                                    <a href="{{ route('products.edit_stock', ['id' =>$product->id]) }}">
                                        @if($product->stock == 0)
                                            <span class="text-md px-3 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold">Out of Stock</span>
                                        @elseif($product->stock <= 10 )
                                            <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold">{{ $product->stock }} In Stock</span>
                                        @else
                                            <span class="text-md px-3 py-1 rounded-md bg-green-500 text-indigo-50 font-semibold">{{ $product->stock }} In Stock</span>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>

                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>