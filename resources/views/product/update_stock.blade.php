<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">Edit Product</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        @if($stock_list != [])
            <form method="POST" action="{{ route('products.update_stock', ['id' => $id]) }}">
                @csrf

                @foreach($stock_list as $stock)
                <div>
                    <p>{{ $stock['name'] }}
                    <label for="{{ strval($stock['id']) . 'location' . strval($stock['location_id']) }}" class="block font-medium text-sm text-gray-700">{{ $stock['location_name'] }}<label/>
                        <x-input id="{{ strval($stock['id']) . 'location' . strval($stock['location_id']) }}" class="block mt-1 w-full" type="number" name="{{ strval($stock['id']) . 'location' . strval($stock['location_id']) }}" :value="$stock['stock_level']" required autofocus />
                </div>

                @endforeach
                <div class="flex items-center justify-end mt-4">

                    <x-button class="ml-4">
                        {{ __('Update Stock Levels') }}
                    </x-button>
                </div>
            </form>
        @else
            <p>This product has no variations. Please add one to add stock</p>
        @endif

    </x-auth-card>
</x-app-layout>
