<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
    <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">New Product</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required />
            </div>

            <!-- Image -->
            <div class="mt-4">
                <x-label for="image" :value="__('Image')" />

                <x-input id="image" class="block mt-1 w-full" type="file" name="image" :value="old('image')" accept=".jpg, .jpeg, .png"/>
            </div>

            <!-- Description -->
            <div class="mt-4">
                <x-label for="description" :value="__('Description')" />

                <textarea id="description" class="block mt-1 w-full" name="description" rows="10" cols="30">    
                </textarea>
            </div>

            <!-- Category -->
            <div class="mt-4">
                <x-label for="category" :value="__('Category')" />

                <x-input id="category" class="block mt-1 w-full" type="text" name="category" :value="old('category')" required />
            </div>

            <!-- Vendor -->
            <div class="mt-4">
                <x-label for="vendor" :value="__('Vendor')" />

                <x-input id="vendor" class="block mt-1 w-full" type="text" name="vendor" :value="old('vendor')" required />
            </div>

            <!-- Low Stock Threshold -->
            <div class="mt-4">
                <x-label for="low_stock_threshold" :value="__('Low Stock Threshold')" />

                <x-input id="low_stock_threshold" class="block mt-1 w-full" type="text" name="low_stock_threshold" :value="old('low_stock_threshold')" required />
            </div>


            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Create Product') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
