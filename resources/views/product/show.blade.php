<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">View Product</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>
        <div class="mb-4">
            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Name</p>

                <p>{{ $product->name }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Description</p>

                <p>{{ $product->description }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Category</p>

                <p>{{ $product->category }}<p/>
            </div>

            <div class="mb-2">
                <p class="block font-medium text-lg text-gray-700">Vendor</p>

                <p>{{ $product->vendor }}<p/>
            </div>

            <div class="mb-2">
                <a href="{{ route('products.edit', ['id' => $product->id]) }}">
                    <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold cursor-pointer">Edit Product</span>
                </a>
            </div>
        </div>   
            <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">Product Variations</h2>

            <div class="mt-4">
                @if($variations != [])
                    @foreach($variations as $variation)
                    <div class="grid grid-cols-2 items-center mb-2">
                                    <div class="mb-2 col-span-2">
                                        <p class="text-md font-medium">{{ $variation['name'] }}
                                            <a href="{{ route('products.edit_stock', ['id' =>$product->id]) }}">
                                                @if($variation['stock'] == 0)
                                                    <span class="text-md px-3 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold">Out of Stock</span>
                                                @elseif($variation['stock'] <= $product->low_stock_threshold )
                                                    <span class="text-md px-3 py-1 rounded-md bg-yellow-500 text-indigo-50 font-semibold">{{ $variation['stock'] }} In Stock</span>
                                                @else
                                                    <span class="text-md px-3 py-1 rounded-md bg-green-500 text-indigo-50 font-semibold">{{ $variation['stock'] }} In Stock</span>
                                                @endif
                                            </a>
                                        </p>
                                    </div>
                                </div>
                    @endforeach
                @else
                    <p>This product currently has no variations</p>
                @endif
            </div>
    </x-auth-card>
</x-app-layout>
