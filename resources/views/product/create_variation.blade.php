<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">New Product Variant</h2>
        <x-slot name="logo">
            <a href="/">
                <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('products.create_variation', ['id' => $product->id]) }}">
            @csrf

            <!-- SKU -->
            <div>
                <x-label for="sku" :value="__('Sku')" />

                <x-input id="sku" class="block mt-1 w-full" type="text" name="sku" :value="old('sku')" required autofocus />
            </div>

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required />
            </div>

            <!-- Description -->
            <div class="mt-4">
                <x-label for="description" :value="__('Description')" />

                <textarea id="description" class="block mt-1 w-full" name="description" rows="10" cols="30">    
                </textarea>
            </div>


            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-4">
                    {{ __('Create Product Child') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-app-layout>
