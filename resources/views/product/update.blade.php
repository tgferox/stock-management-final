<x-app-layout>
    <x-slot name="header">
    </x-slot>
    <x-auth-card>
        <h2 class="font-semibold text-xl text-gray-800 text-center leading-tight">Edit Product</h2>
        <x-slot name="logo">
            <a href="/">
            <p>Logo</p>
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('products.update', ['id' => $product->id]) }}" enctype="multipart/form-data">
            @csrf

            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$product->name" required />
            </div>

            <!-- Image -->
            <div class="mt-4">
                
                <x-label for="image" :value="__('Image')" />
                <img src="{{ asset($product->image) }}" alt="{{ $product->name }}"/>
                <x-input id="image" class="block mt-1 w-full" type="file" name="image" :value="$product->image" accept=".jpg, .jpeg, .png"/>
            </div>

            <!-- Description -->
            <div class="mt-4">
                <x-label for="description" :value="__('Description')" />

                <textarea id="description" class="block mt-1 w-full" name="description" rows="10" cols="30">   
                {{ $product->description }}
                </textarea>
            </div>

            <!-- Category -->
            <div class="mt-4">
                <x-label for="category" :value="__('Category')" />

                <x-input id="category" class="block mt-1 w-full" type="text" name="category" :value="$product->category" />
            </div>

            <!-- Vendor -->
            <div class="mt-4">
                <x-label for="low_stock_threshold" :value="__('Low Stock Threshold')" />

                <x-input id="low_stock_threshold" class="block mt-1 w-full" type="text" name="low_stock_threshold" :value="$product->low_stock_threshold" required />
            </div>

            <div class="mt-4">
                <x-label for="vendor" :value="__('Vendor')" />

                <x-input id="vendor" class="block mt-1 w-full" type="text" name="vendor" :value="$product->vendor" required />
            </div>

            <div class="mt-4">
                <x-label for="variations" :value="__('Variations')" />
                @if($variations != [])
                    @foreach($variations as $variation)
                        <div>
                            <x-label for="{{ $variation['id'] }}" :value="$variation['name']" />

                                <!-- SKU -->
                                <div class="mt-4">
                                    <x-label for="{{ strval($variation['id']) . 'sku'}}" :value="__('SKU')" />

                                    <x-input id="{{ strval($variation['id']) . 'sku'}}" class="block mt-1 w-full" type="text" name="{{ strval($variation['id']) . 'sku'}}" :value="$variation['sku']" required />
                                </div>

                                <!-- Name -->
                                <div class="mt-4">
                                    <x-label for="{{ strval($variation['id']) . 'name'}}" :value="__('Name')" />

                                    <x-input id="{{ strval($variation['id']) . 'name'}}" class="block mt-1 w-full" type="text" name="{{ strval($variation['id']) . 'name'}}" :value="$variation['name']" required />
                                </div>

                                <!-- Description -->
                                <div class="mt-4">
                                    <x-label for="{{ strval($variation['id']) . 'description'}}" :value="__('Description')" />

                                    <textarea id="{{ strval($variation['id']) . 'description'}}" class="block mt-1 w-full" name="{{ strval($variation['id']) . 'description'}}" rows="10" cols="30">   
                                    {{ $variation['description'] }}
                                    </textarea>
                                </div>
                        </div>
                    @endforeach
                @else
                    <p>This product currently has no variations</p>
                @endif
            </div>
            <div class="flex items-center justify-end mt-4">

                <x-button class="text-md px-3 py-1 rounded-md bg-gray-500 text-indigo-50 font-semibold cursor-pointer">
                    {{ __('Update Product') }}
                </x-button>
            </div>
        </form>
        <div class="flex items-center justify-end mt-4">
            <a href="{{ route('products.new_variation', ['id' => $product->id]) }}">
                <button class="text-md px-3 py-1 rounded-md bg-gray-500 text-indigo-50 font-semibold cursor-pointer" >
                    Add Child
                </button>
            </a>
        </div>
        <div class="flex items-center justify-end mt-4">
            <a href="{{ route('products.edit_stock', ['id' => $product->id]) }}">
                <button class="text-md px-3 py-1 rounded-md bg-gray-500 text-indigo-50 font-semibold cursor-pointer" >
                    Edit Stock
                </button>
            </a>
        </div>
        <div class="flex items-center justify-end mt-4">
            <form action="{{ route('products.delete', ['id' => $product->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button class="text-md px-6 py-1 rounded-md bg-red-500 text-indigo-50 font-semibold cursor-pointer" title="Delete">Delete</button>
            </form>
        </div>
    </x-auth-card>
</x-app-layout>
